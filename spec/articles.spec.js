const { MOCKED_ARTICLE, NEW_ARTICLE_BASE } = require('../data/articles');
const { setup, teardown } = require('./helper');
const { assertFails, assertSucceeds } = require('@firebase/testing');

describe('Articles rules', () => {
  let mockAuth;
  let mockedArticlesData;
  let newArticleBase;

  beforeEach(async () => {
    mockAuth = {
      uid: '1',
      admin: true
    };

    mockedArticlesData = {
      'articles/all/pubished/1': {
        ...MOCKED_ARTICLE
      },
    };

    newArticleBase = {
      ...NEW_ARTICLE_BASE
    }
  });

  afterEach(async () => {
    await teardown();
  });

  describe('Futbolbro - articles', () => {
    it('should fails to read', async () => {
      const db = await setup(mockAuth, mockedArticlesData);
      const ref = db.collection('articles');

      await assertFails(ref.get());
    });

    it('should fails to write', async () => {
      const db = await setup(mockAuth, mockedArticlesData);
      const newArticle = {
        ...newArticleBase
      }
      const ref = db.collection('articles').doc('2');

      await assertFails(ref.set(newArticle));
    });
  });

  describe('Futbolbro - articles/all', () => {
    it('should fails to read', async () => {
      const db = await setup(mockAuth, mockedArticlesData);
      const ref = db.collection('articles').doc('all');

      await assertFails(ref.get());
    });

    it('should fails to write', async () => {
      const db = await setup(mockAuth, mockedArticlesData);
      const newArticle = {
        ...newArticleBase
      }
      const ref = db.collection('articles').doc('all');

      await assertFails(ref.set(newArticle));
    });
  });

  describe('Futbolbro - articles/all/published', () => {
    it('should succeed to get all articles', async () => {
      const db = await setup(mockAuth, mockedArticlesData);
      const ref = db.collection('articles/all/published');

      await assertSucceeds(ref.get());
    });

    it('should succeed to get a article', async () => {
      const db = await setup(mockAuth, mockedArticlesData);
      const ref = db.collection('articles/all/published').doc('1');

      await assertSucceeds(ref.get());
    });

    it('should fails to create a article if logged user is not a admin', async () => {
      const loggedUser = {
        ...mockAuth
      };
      loggedUser.admin = false;
      const db = await setup(loggedUser, mockedArticlesData);
      const newArticle = {
        ...newArticleBase
      };
      const ref = db.collection('articles/all/published').doc('2');

      await assertFails(ref.set(newArticle));
    });

    it('should fails to create a article if the title is not a string', async () => {
      const db = await setup(mockAuth, mockedArticlesData);
      const newArticle = {
        ...newArticleBase
      }
      newArticle.title = 1234;
      const ref = db.collection('articles/all/published').doc('2');

      await assertFails(ref.set(newArticle));
    });

    it('should fails to create a article if the title is less than 5 symbols long', async () => {
      const db = await setup(mockAuth, mockedArticlesData);
      const newArticle = {
        ...newArticleBase
      }
      newArticle.title = '1234';
      const ref = db.collection('articles/all/published').doc('2');

      await assertFails(ref.set(newArticle));
    });

    it('should fails to create a article if the title more than 150 symbols long', async () => {
      const db = await setup(mockAuth, mockedArticlesData);
      const newArticle = {
        ...newArticleBase
      }
      newArticle.title = new Array(151).fill('s');
      const ref = db.collection('articles/all/published').doc('2');

      await assertFails(ref.set(newArticle));
    });

    it('should fails to create a article if the content is not a string', async () => {
      const db = await setup(mockAuth, mockedArticlesData);
      const newArticle = {
        ...newArticleBase
      }
      newArticle.content = 1234;
      const ref = db.collection('articles/all/published').doc('2');

      await assertFails(ref.set(newArticle));
    });

    it('should fails to create a article if the content is less than 150 symbols long', async () => {
      const db = await setup(mockAuth, mockedArticlesData);
      const newArticle = {
        ...newArticleBase
      }
      newArticle.content = new Array(149).fill('s');;
      const ref = db.collection('articles/all/published').doc('2');

      await assertFails(ref.set(newArticle));
    });

    it('should fails to create a article if the content more than 50000 symbols long', async () => {
      const db = await setup(mockAuth, mockedArticlesData);
      const newArticle = {
        ...newArticleBase
      }
      newArticle.content = new Array(50001).fill('s');
      const ref = db.collection('articles/all/published').doc('2');

      await assertFails(ref.set(newArticle));
    });

    it('should fails to create a article if the image is not a string', async () => {
      const db = await setup(mockAuth, mockedArticlesData);
      const newArticle = {
        ...newArticleBase
      }
      newArticle.image = 1234;
      const ref = db.collection('articles/all/published').doc('2');

      await assertFails(ref.set(newArticle));
    });

    it('should fails to create a article if the author is not a map', async () => {
      const db = await setup(mockAuth, mockedArticlesData);
      const newArticle = {
        ...newArticleBase
      }
      newArticle.author = 'test';
      const ref = db.collection('articles/all/published').doc('2');

      await assertFails(ref.set(newArticle));
    });

    it('should fails to create a article if the author.uid is not a string', async () => {
      const db = await setup(mockAuth, mockedArticlesData);
      const newArticle = {
        ...newArticleBase
      }
      newArticle.author.uid = 1234;
      const ref = db.collection('articles/all/published').doc('2');

      await assertFails(ref.set(newArticle));
    });

    it('should fails to create a article if the author.username is not a string', async () => {
      const db = await setup(mockAuth, mockedArticlesData);
      const newArticle = {
        ...newArticleBase
      }
      newArticle.author.username = 1234;
      const ref = db.collection('articles/all/published').doc('2');

      await assertFails(ref.set(newArticle));
    });

    it('should fails to create a article if the author.avatar is not a string', async () => {
      const db = await setup(mockAuth, mockedArticlesData);
      const newArticle = {
        ...newArticleBase
      }
      newArticle.author.avatar = 1234;
      const ref = db.collection('articles/all/published').doc('2');

      await assertFails(ref.set(newArticle));
    });

    it('should fails to create a article if the author map has more than needed props', async () => {
      const db = await setup(mockAuth, mockedArticlesData);
      const newArticle = {
        ...newArticleBase
      }
      newArticle.author = {
        uid: '1',
        username: 'testov',
        avatar: 'default',
        test: 'test'
      };
      const ref = db.collection('articles/all/published').doc('2');

      await assertFails(ref.set(newArticle));
    });

    it('should fails to create a article if the createdOn is not a timestamp', async () => {
      const db = await setup(mockAuth, mockedArticlesData);
      const newArticle = {
        ...newArticleBase
      }
      newArticle.createdOn = 1234;
      const ref = db.collection('articles/all/published').doc('2');

      await assertFails(ref.set(newArticle));
    });

    it('should fails to create a article if the category is not a string', async () => {
      const db = await setup(mockAuth, mockedArticlesData);
      const newArticle = {
        ...newArticleBase
      }
      newArticle.category = 1234;
      const ref = db.collection('articles/all/published').doc('2');

      await assertFails(ref.set(newArticle));
    });

    it('should fails to create a article if the status is not a string', async () => {
      const db = await setup(mockAuth, mockedArticlesData);
      const newArticle = {
        ...newArticleBase
      }
      newArticle.status = 1234;
      const ref = db.collection('articles/all/published').doc('2');

      await assertFails(ref.set(newArticle));
    });

    it('should fails to create a article if the status is not equal Публикувана', async () => {
      const db = await setup(mockAuth, mockedArticlesData);
      const newArticle = {
        ...newArticleBase
      }
      newArticle.status = '1234';
      const ref = db.collection('articles/all/published').doc('2');

      await assertFails(ref.set(newArticle));
    });

    it('should succeed to create a article if all the props are correct and the logged user is admin', async () => {
      const db = await setup(mockAuth, mockedArticlesData);
      const ref = db.collection('articles/all/published').doc('2');

      await assertSucceeds(ref.set(newArticleBase));
    });
  });
});
