const firebase = require('@firebase/testing');
const fs = require('fs');

module.exports.setup = async (auth = null, data = []) => {
  const projectId = `rules-spec-${Date.now()}`;
  const app = firebase.initializeTestApp({
    projectId,
    auth,
  });

  const db = app.firestore();

  // Write mock document before rules
  if (data) {
    for (const key in data) {
      if (key) {
        const ref = db.doc(key);
        await ref.set(data[key]);
      }
    }
  }

  await firebase.loadFirestoreRules({
    projectId,
    rules: fs.readFileSync('firestore.rules', 'utf-8')
  });

  return db;
};

module.exports.teardown = async () => {
  await Promise.all(firebase.apps().map(async (app) => await app.delete()));
};
