const { MOCKED_USER, NEW_USER_BASE } = require('../data/users');
const { setup, teardown } = require('./helper');
const { assertFails, assertSucceeds } = require('@firebase/testing');

describe('Users rules', () => {
  let mockAuth;
  let mockedUsersData;
  let newUserBase;

  beforeEach(async () => {
    mockAuth = {
      uid: '2',
    };

    mockedUsersData = {
      'users/1': {
        ...MOCKED_USER
      },
    };

    newUserBase = {
      ...NEW_USER_BASE
    }
  });

  afterEach(async () => {
    await teardown();
  });

  describe('Futbolbro', () => {
    it('should fail to get all the users when there is no logged user', async () => {
      const db = await setup(null, mockedUsersData);
      const ref = db.collection('users');
  
      await assertFails(ref.get());
    });
  
    it('should fail to get a user when there is no logged user', async () => {
      const db = await setup(null, mockedUsersData);
      const ref = db.collection('users').doc('1');;
  
      await assertFails(ref.get());
    });
  
    it('should succeed to get all the users when there is logged user', async () => {
      const db = await setup(mockAuth, mockedUsersData);
      const ref = db.collection('users');
  
      await assertSucceeds(ref.get());
    });
  
    it('should succeed to get a user when there is logged user', async () => {
      const db = await setup(mockAuth, mockedUsersData);
      const ref = db.collection('users').doc('1');

      await assertSucceeds(ref.get());
    });
  
    it('should fail to create a new user when there is logged user', async () => {
      const db = await setup(mockAuth, mockedUsersData);
      const newUser = {
        ...newUserBase
      };
      const ref = db.collection('users').doc('3');

      await assertFails(ref.set(newUser));
    });
  
    it('should succeed to create a new user when all properties are correct', async () => {
      const db = await setup(null, mockedUsersData);
      const newUser = {
        ...newUserBase
      };
      const ref = db.collection('users').doc('3');

      await assertSucceeds(ref.set(newUser));
    });
  
    it('should fail to create a new user if the username size is less than 5 symbols long', async () => {
      const db = await setup(null, mockedUsersData);
      const newUser = {
        ...newUserBase
      };
      newUser.username = '1234';
      const ref = db.collection('users').doc('3');
  
      await assertFails(ref.set(newUser));
    });
  
    it('should fail to create a new user if the username size is more than 15 symbols long', async () => {
      const db = await setup(null, mockedUsersData);
      const newUser = {
        ...newUserBase
      };
      newUser.username = '01234567890123456';
      const ref = db.collection('users').doc('3');
  
      await assertFails(ref.set(newUser));
    });
  
    it('should fail to create a new user if the username is not a string', async () => {
      const db = await setup(null, mockedUsersData);
      const newUser = {
        ...newUserBase
      };
      newUser.username = 123456;
      const ref = db.collection('users').doc('3');
  
      await assertFails(ref.set(newUser));
    });

    it('should fail to create a new user if the uid is not a string', async () => {
      const db = await setup(null, mockedUsersData);
      const newUser = {
        ...newUserBase
      };
      newUser.uid = 2;
      const ref = db.collection('users').doc('3');
  
      await assertFails(ref.set(newUser));
    });

    it('should fail to create a new user if the avatar is not equal to default', async () => {
      const db = await setup(null, mockedUsersData);
      const newUser = {
        ...newUserBase
      };
      newUser.avatar = 'test';
      const ref = db.collection('users').doc('3');
  
      await assertFails(ref.set(newUser));
    });

    it('should fail to create a new user if the description is not equal to Все още нямам описание', async () => {
      const db = await setup(null, mockedUsersData);
      const newUser = {
        ...newUserBase
      };
      newUser.description = '1234';
      const ref = db.collection('users').doc('3');
  
      await assertFails(ref.set(newUser));
    });

    it('should fail to create a new user if the favouriteArticles is not a list', async () => {
      const db = await setup(null, mockedUsersData);
      const newUser = {
        ...newUserBase
      };
      newUser.favouriteArticles = 'test';
      const ref = db.collection('users').doc('3');
  
      await assertFails(ref.set(newUser));
    });

    it('should fail to create a new user if the favouriteArticles size is not 0', async () => {
      const db = await setup(null, mockedUsersData);
      const newUser = {
        ...newUserBase
      };
      newUser.favouriteArticles = ['1'];
      const ref = db.collection('users').doc('3');
  
      await assertFails(ref.set(newUser));
    });

    it('should fail to create a new user if the comments is not list', async () => {
      const db = await setup(null, mockedUsersData);
      const newUser = {
        ...newUserBase
      };
      newUser.comments = 'test';
      const ref = db.collection('users').doc('3');
  
      await assertFails(ref.set(newUser));
    });

    it('should fail to create a new user if the comments size is not 0', async () => {
      const db = await setup(null, mockedUsersData);
      const newUser = {
        ...newUserBase
      };
      newUser.comments = ['1'];
      const ref = db.collection('users').doc('3');
  
      await assertFails(ref.set(newUser));
    });
  
    it('should fail to create a new user if the favouriteTeam is not a map', async () => {
      const db = await setup(null, mockedUsersData);
      const newUser = {
        ...newUserBase
      };
      newUser.favouriteTeam = 'test';
      const ref = db.collection('users').doc('3');
  
      await assertFails(ref.set(newUser));
    });
  
    it('should fail to create a new user if the favouriteTeam.mainColor is not a string', async () => {
      const db = await setup(null, mockedUsersData);
      const newUser = {
        ...newUserBase
      };
      newUser.favouriteTeam.mainColor = null;
      const ref = db.collection('users').doc('3');
  
      await assertFails(ref.set(newUser));
    });
  
    it('should fail to create a new user if the favouriteTeam.secondaryColor is not a string', async () => {
      const db = await setup(null, mockedUsersData);
      const newUser = {
        ...newUserBase
      };
      newUser.favouriteTeam.secondaryColor = null;
      const ref = db.collection('users').doc('3');

      await assertFails(ref.set(newUser));
    });
  
    it('should fail to create a new user if the favouriteTeam.name is not a string', async () => {
      const db = await setup(null, mockedUsersData);
      const newUser = {
        ...newUserBase
      };
      newUser.favouriteTeam.name = null;
      const ref = db.collection('users').doc('3');
  
      await assertFails(ref.set(newUser));
    });

    it('should fail to create a new user if the favouriteTeam has more than the needed keys', async () => {
      const db = await setup(null, mockedUsersData);
      const newUser = {
        ...newUserBase
      };
      newUser.favouriteTeam = {
        secondaryColor: 'dd0000',
        mainColor: 'dd0000',
        name: 'Ливърпул',
        test: 'test'
      };
      const ref = db.collection('users').doc('3');
  
      await assertFails(ref.set(newUser));
    });
  
    it('should fail to create a new user if the terms is not true', async () => {
      const db = await setup(null, mockedUsersData);
      const newUser = {
        ...newUserBase
      };
      newUser.terms = false;
      const ref = db.collection('users').doc('3');
  
      await assertFails(ref.set(newUser));
    });

    it('should fail to create a new user if the isDeleted is not false', async () => {
      const db = await setup(null, mockedUsersData);
      const newUser = {
        ...newUserBase
      };
      newUser.terms = [];
      const ref = db.collection('users').doc('3');
  
      await assertFails(ref.set(newUser));
    });
  
    it('should fail to create a new user if the predictions is not a map', async () => {
      const db = await setup(null, mockedUsersData);
      const newUser = {
        ...newUserBase
      };
      newUser.predictions = 'test';
      const ref = db.collection('users').doc('3');
  
      await assertFails(ref.set(newUser));
    });
  
    it('should fail to create a new user if the predictions.total is not 0', async () => {
      const db = await setup(null, mockedUsersData);
      const newUser = {
        ...newUserBase
      };
      newUser.predictions.total = null;
      const ref = db.collection('users').doc('3');
  
      await assertFails(ref.set(newUser));
    });
  
    it('should fail to create a new user if the predictions.successful is not 0', async () => {
      const db = await setup(null, mockedUsersData);
      const newUser = {
        ...newUserBase
      };
      newUser.predictions.successful = null;
      const ref = db.collection('users').doc('3');

      await assertFails(ref.set(newUser));
    });
  
    it('should fail to create a new user if the predictions.failed is not 0', async () => {
      const db = await setup(null, mockedUsersData);
      const newUser = {
        ...newUserBase
      };
      newUser.predictions.failed = null;
      const ref = db.collection('users').doc('3');
  
      await assertFails(ref.set(newUser));
    });

    it('should fail to create a new user if the predictions is missing any of the needed keys', async () => {
      const db = await setup(null, mockedUsersData);
      const newUser = {
        ...newUserBase
      };
      newUser.favouriteTeam = {
        total: 0,
      };
      const ref = db.collection('users').doc('3');
  
      await assertFails(ref.set(newUser));
    });

    it('should fail to create a new user if the rank is not a map', async () => {
      const db = await setup(null, mockedUsersData);
      const newUser = {
        ...newUserBase
      };
      newUser.rank = null
      const ref = db.collection('users').doc('3');
  
      await assertFails(ref.set(newUser));
    });

    it('should fail to create a new user if the rank.currentPoints is not 1', async () => {
      const db = await setup(null, mockedUsersData);
      const newUser = {
        ...newUserBase
      };
      newUser.rank.currentPoints = 2;
      const ref = db.collection('users').doc('3');
  
      await assertFails(ref.set(newUser));
    });

    it('should fail to create a new user if the rank.currentRankPosition is not 0', async () => {
      const db = await setup(null, mockedUsersData);
      const newUser = {
        ...newUserBase
      };
      newUser.rank.currentRankPosition = 2;
      const ref = db.collection('users').doc('3');
  
      await assertFails(ref.set(newUser));
    });

    it('should fail to create a new user if the rank.nextRankPoints is not 10', async () => {
      const db = await setup(null, mockedUsersData);
      const newUser = {
        ...newUserBase
      };
      newUser.rank.nextRankPoints = 2;
      const ref = db.collection('users').doc('3');
  
      await assertFails(ref.set(newUser));
    });

    it('should fail to create a new user if the rank.currentRankName is not equal to Юноша', async () => {
      const db = await setup(null, mockedUsersData);
      const newUser = {
        ...newUserBase
      };
      newUser.rank.currentRankName = 'Резерва';
      const ref = db.collection('users').doc('3');
  
      await assertFails(ref.set(newUser));
    });

    it('should fail to create a new user if the rank.currentRankIcon is not equal to Юноша', async () => {
      const db = await setup(null, mockedUsersData);
      const newUser = {
        ...newUserBase
      };
      newUser.rank.currentRankIcon = 'substitution';
      const ref = db.collection('users').doc('3');
  
      await assertFails(ref.set(newUser));
    });

    it('should fail to create a new user if the rank has more than needed keys', async () => {
      const db = await setup(null, mockedUsersData);
      const newUser = {
        ...newUserBase
      };
      newUser.rank = {
        nextRankName: 'Резерва',
        nextRankPoints: 10,
        nextRankIcon: 'substitution',
        currentRankName: 'Юноша',
        currentRankPosition: 0,
        currentRankIcon: 'teen',
        currentPoints: 1,
        test: 'test'
      };
      const ref = db.collection('users').doc('3');
  
      await assertFails(ref.set(newUser));
    });

    it('should fail to create a new user if the rank.nextRankName is not equal to Резерва', async () => {
      const db = await setup(null, mockedUsersData);
      const newUser = {
        ...newUserBase
      };
      newUser.rank.nextRankName = 'Юноша';
      const ref = db.collection('users').doc('3');
  
      await assertFails(ref.set(newUser));
    });

    it('should fail to create a new user if the rank.nextRankIcon is not equal to substitution', async () => {
      const db = await setup(null, mockedUsersData);
      const newUser = {
        ...newUserBase
      };
      newUser.rank.nextRankIcon = 'teen';
      const ref = db.collection('users').doc('3');
  
      await assertFails(ref.set(newUser));
    });

    it('should fail to create a new user if the createdOn is not a timestamp', async () => {
      const db = await setup(null, mockedUsersData);
      const newUser = {
        ...newUserBase
      };
      newUser.createdOn = 'test';
      const ref = db.collection('users').doc('3');
  
      await assertFails(ref.set(newUser));
    });
  
    it('should fail to update a user if there is no authenticated user', async () => {
      const db = await setup(null, mockedUsersData);
      const ref = db.collection('users').doc('3');

      await assertFails(ref.update({'description': 'This is a test'}));
    });
  
    it('should fail to update a user if authenticated user is not admin or owner', async () => {
      const db = await setup(mockAuth, mockedUsersData);
      const ref = db.collection('users').doc('1');
  
      await assertFails(ref.update({description: 'This is a test'}));
    });
  
    it('should succeed to update a user if the authenticated user is admin', async () => {
      const mockAuthUpdate = {
        uid: '2',
        admin: true
      };
      const db = await setup(mockAuthUpdate, mockedUsersData);
      const ref = db.collection('users').doc('1');

      await assertSucceeds(ref.update({description: 'This is a test'}));
    });
  
    it('should succeed to update a user if authenticated user is owner', async () => {
      const mockAuthUpdate = {
        uid: '1',
      };
      const db = await setup(mockAuthUpdate, mockedUsersData);
      const ref = db.collection('users').doc('1');
      
      await assertSucceeds(ref.update({description: 'This is a test'}));
    });

    it('should fail to update a user if the user does not exist', async () => {
      const mockAuthUpdate = {
        uid: '3',
        admin: true
      };
      const db = await setup(mockAuthUpdate, mockedUsersData);
      const ref = db.collection('users').doc('4');
      
      await assertFails(ref.update({description: 'This is a test'}));
    });

    it('should fail to update a user if trying to update the username', async () => {
      const mockAuthUpdate = {
        uid: '3',
        admin: true
      };
      const db = await setup(mockAuthUpdate, mockedUsersData);
      const ref = db.collection('users').doc('3');
      
      await assertFails(ref.update({username: 'testUsername'}));
    });

    it('should fail to update a user if the description does not fulfill the constraints', async () => {
      const mockAuthUpdate = {
        uid: '3',
        admin: true
      };
      const db = await setup(mockAuthUpdate, mockedUsersData);
      const ref = db.collection('users').doc('3');
      
      await assertFails(ref.update({description: 1234}));
    });

    it('should succeed to update a user if the description does fulfill the constraints', async () => {
      const mockAuthUpdate = {
        uid: '3',
        admin: true
      };
      const db = await setup(mockAuthUpdate, mockedUsersData);
      const ref = db.collection('users').doc('3');
      
      await assertFails(ref.update({description: 'My great description'}));
    });

    it('should fail to update a user if the avatar does not fulfill the constraints', async () => {
      const mockAuthUpdate = {
        uid: '3',
        admin: true
      };
      const db = await setup(mockAuthUpdate, mockedUsersData);
      const ref = db.collection('users').doc('3');
      
      await assertFails(ref.update({avatar: 1234}));
    });

    it('should succeed to update a user if the avatar does fulfill the constraints', async () => {
      const mockAuthUpdate = {
        uid: '3',
        admin: true
      };
      const db = await setup(mockAuthUpdate, mockedUsersData);
      const ref = db.collection('users').doc('3');
      
      await assertFails(ref.update({avatar: '50'}));
    });

    it('should fail to update a user if the isDeleted does not fulfill the constraints', async () => {
      const mockAuthUpdate = {
        uid: '3',
        admin: true
      };
      const db = await setup(mockAuthUpdate, mockedUsersData);
      const ref = db.collection('users').doc('3');
      
      await assertFails(ref.update({isDeleted: 1234}));
    });

    it('should succeed to update a user if the isDeleted does fulfill the constraints', async () => {
      const mockAuthUpdate = {
        uid: '3',
        admin: true
      };
      const db = await setup(mockAuthUpdate, mockedUsersData);
      const ref = db.collection('users').doc('3');
      
      await assertFails(ref.update({isDeleted: true}));
    });

    it('should fail to update a user if trying to update the predictions', async () => {
      const mockAuthUpdate = {
        uid: '3',
        admin: true
      };
      const db = await setup(mockAuthUpdate, mockedUsersData);
      const ref = db.collection('users').doc('3');

      const predictions = {
        total: 0,
        successful: 0,
        failed: 0,
      }
      
      await assertFails(ref.update({predictions}));
    });

    it('should fail to delete a user document if the logged user is not admin', async () => {
      const mockAuthUpdate = {
        uid: '3'
      };
      const db = await setup(mockAuthUpdate, mockedUsersData);
      const ref = db.collection('users').doc('3');
      
      await assertFails(ref.delete());
    });
  });
});
