const { firestore } = require('@firebase/testing');

const MOCKED_ARTICLE = {
  uid: '1',
  title: 'Роналдо и Меси ще играят в Ювентус от следващия сезон',
  content: new Array(151).fill('a').join(''),
  image: 'default-article-image.jpg',
  author: {
    uid: '1',
    username: 'testTestov',
    avatar: 'default'
  },
  createdOn: firestore.FieldValue.serverTimestamp(),
  category: 'Новини',
  status: 'Публикувана',
};

const NEW_ARTICLE_BASE = {
  uid: '2',
  title: 'Ювентус спечели Шампионска Лига след като надигра ОФК Сливен',
  content: new Array(151).fill('a').join(''),
  image: 'default-article-image.jpg',
  author: {
    uid: '1',
    username: 'testTestov',
    avatar: 'default'
  },
  createdOn: firestore.FieldValue.serverTimestamp(),
  category: 'Анализи',
  status: 'Публикувана',
};

exports.MOCKED_ARTICLE = MOCKED_ARTICLE;
exports.NEW_ARTICLE_BASE = NEW_ARTICLE_BASE;