const MOCKED_USER = {
  avatar: 'default',
  comments: [],
  createdOn: new Date(),
  description: 'Все още нямам описание',
  email: 'testov@testov.bg',
  favouriteArticles: [],
  favouriteTeam: {
    secondaryColor: 'dd0000',
    mainColor: 'dd0000',
    name: 'Ливърпул'
  },
  isDeleted: false,
  predictions: {
    total: 0,
    failed: 0,
    successful: 0
  },
  rank: {
    nextRankName: 'Резерва',
    nextRankPoints: 10,
    nextRankIcon: 'substitution',
    currentRankName: 'Юноша',
    currentRankPosition: 0,
    currentRankIcon: 'teen',
    currentPoints: 1
  },
  terms: true,
  uid: '1',
  username: 'testovtest',
};

const NEW_USER_BASE = {
  avatar: 'default',
  comments: [],
  createdOn: new Date(),
  description: 'Все още нямам описание',
  email: 'mock@mockov.com',
  favouriteArticles: [],
  favouriteTeam: {
    secondaryColor: 'dd0000',
    mainColor: 'dd0000',
    name: 'Ливърпул'
  },
  isDeleted: false,
  predictions: {
    total: 0,
    failed: 0,
    successful: 0
  },
  rank: {
    nextRankName: 'Резерва',
    nextRankPoints: 10,
    nextRankIcon: 'substitution',
    currentRankName: 'Юноша',
    currentRankPosition: 0,
    currentRankIcon: 'teen',
    currentPoints: 1
  },
  terms: true,
  uid: '4',
  username: 'mockmockov',
};

exports.MOCKED_USER = MOCKED_USER;
exports.NEW_USER_BASE = NEW_USER_BASE;